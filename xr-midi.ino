/**
  * Copyright 2024 Rainer Jung
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

// Setup MIDI-Messages to be sent
const int count = 1;
char *messages[] = {
  "Pin 1"  // Will be something like "\x39\x00\x00" (a midi message)
};
// Setup the pins for the MIDI-Messages
int pins[count] = {
  PIN2
};

// Internal memory of the pin states
bool previous[count] = {
  false
};

void setup() {
  // Midi speed is 31250 Bits/s
  Serial.begin(31250);

  // Initialize all pins and current status of the pins
  for (int i = 0; i < count; i++) {
    // Set PIN to read
    pinMode(pins[i], INPUT_PULLUP);
    // Store current input of pin
    previous[i] = digitalRead(pins[i]);
  }
}

void loop() {
  // Check all pins
  for (int i = 0; i < count; i++) {
    // Read the current value
    bool newValue = digitalRead(pins[i]);
    // Compare it with the previous input
    if (newValue != previous[i]) {
      // Send corresponsing message
      Serial.write(messages[i]);
      // Store new value of pin
      previous[i] = newValue;
    }
  }
}
