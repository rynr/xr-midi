XR-Midi
=======

Remote Control a XR18 via a MIDI-Footpedal.

But the code can easily be updated to control any midi device.

Configuration
-------------

3 variables have to be aligned with your needs.

```c
const int count = 3;
```

The variable `count` defines how many switches are used. According to this
number, the next two variables need to be configured.

```c
int pins[count] = { PIN2, PIN3, PIN5 };
```

The variable `pins` is an array of the pins used. The order of the pins is only
relevant to the `messages` variable, there's no need to have any order. The
count of pins needs to align with the value of `count`.

```c
char *messages[] = { "\xc1\x00", "\xc1\x01", "\xc1\x02" };
```

The variable `messages` assignes the messages to be sent to the `pins`. The
count of entries needs to align with the value of `count`. The order aligns
with the order of the `pins`.

Wiring
------

This code assumes switches, not buttons. If buttons are used, it will send a
message for each button press and for each button release.

One of the switches connector needs to be connected to GND, the other one is to
be connected to the PIN which has been configured.

There are better ways to connect a real midi device, but for testing, you can
connect a DIN-5 plug like it's axplained in this [midi-example][midi-example]:

 * MIDI jack pin 5 connected to Digital pin 1 through a 220 ohm resistor
 * MIDI jack pin 2 connected to ground
 * MIDI jack pin 4 connected to +5V through a 220 ohm resistor

[midi-example]: https://docs.arduino.cc/built-in-examples/communication/Midi/
